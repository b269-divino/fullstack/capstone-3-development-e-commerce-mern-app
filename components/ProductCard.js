import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';


// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';


export default function ProductCard({ product }) {

  const { _id, name, description, price, isActive, createdOn } = product;

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title><h4>{name}</h4></Card.Title>
            <Card.Subtitle>Description111</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
{/*            <Card.Subtitle>isActive</Card.Subtitle>
            <Card.Text>{isActive ? "Yes" : "No"}</Card.Text>
            <Card.Subtitle>Created On</Card.Subtitle>
            <Card.Text>{new Date(createdOn).toDateString()}</Card.Text>
            <Card.Subtitle>Orderaa Count</Card.Subtitle>
            <Card.Text>{orders}</Card.Text>*/}
            <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}
