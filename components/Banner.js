// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';


import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

    const {title, content, destination, label} = data;

return (
    <Row>
        <Col className="p-5">
            <h1>{title}</h1>
            <p>{content}</p>
            {/*<Link to={destination}>{label}</Link>*/}
            <Button variant="primary" as={Link} to={destination} >{label}</Button>

        </Col>
    </Row>
    )
}





// /*S53 activity code start*/

// import { Button, Row, Col } from 'react-bootstrap';

// function Banner(props) {
//   if (props.showEnrollmentBanner) {
//     return (
//       <Row>
//         <Col className="p-5">
//           <h1>Zuitt Coding Bootcamp</h1>
//           <p>Opportunities for everyone, everywhere.</p>
//           <Button variant="primary">Enroll now!</Button>
//         </Col>
//       </Row>
//     );
//   } else {
//     return (
//       <div>
//         <h1>404 Not Found</h1>
//         <p>The page you are looking for does not exist.</p>
//       </div>
//     );
//   }
// }

// function ErrorPage() {
//   return (
//     <Banner showEnrollmentBanner={false} />
//   );
// }

// function App() {
//   return (
//     <div>
//       <Banner showEnrollmentBanner={true} />
//       {/* your other app content here */}
//     </div>
//   );
// }

// export default App;

// /*S53 activity code end*/
